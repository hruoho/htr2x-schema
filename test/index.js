const index = require('../index')

const tap = require('tap')

tap.test('index', t => {
  t.plan(4)
  let capt = index.captain
  t.ok(capt)
  t.ok(typeof capt === 'object')
  t.ok(typeof capt.properties === 'object')
  t.ok(typeof capt.properties.id === 'object')
})
